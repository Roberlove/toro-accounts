'use strict';
const Joi = require('joi')

const createAccountValidator = Joi.object({
  id: Joi.string().length(13).required(),
  cpf: Joi.string().length(14).required(),
  checkingAccountAmount: Joi.number().required(),
  positions: Joi.array().required(),
  consolidated: Joi.number().required()
})

function createAccount (data) {
  return createAccountValidator.validateAsync(data, { abortEarly: false })
}

module.exports = {
    createAccount
}
