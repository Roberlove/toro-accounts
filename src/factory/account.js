function accountFactory(data) {
  return {
    id: data.account,
    cpf: data.cpf,
    checkingAccountAmount: 0,
    positions: [],
    consolidated: 0
  }
}

module.exports = accountFactory