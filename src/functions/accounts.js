'use strict';
const { httpFactory } = require('../factory/responses/httpFactory')
const table = process.env.TB_ACCOUNT
const tablePK = process.env.TB_ACCOUNT_PK
const validator = require('../validators/account')
const { setTableItem, invokeLbda } = require('../services/aws-sevice')
const accountFactory = require('../factory/account')

async function userPosition (event) {
  const token = event.headers.Authorization
  const e = await invokeLbda('tokenValidator', token)
  console.log('Event: ', event)
  const account = {
    checkingAccountAmount: 234.00,
    positions: [
      {
        symbol: "PETR4",
        amount: 2,
        currentPrice: 28.44,
      },
      {
        symbol: "SANB11",
        amount: 3,
        currentPrice: 40.77
      },
    ],
    "consolidated": 413.19
  }
  return httpFactory(account, 200)
}

async function createAccount (event) {
  console.log('Event: ', JSON.stringify(event))
  const body = JSON.parse(event.Records[0].body)
  const account = accountFactory(body)
  await setTableItem(table, account, tablePK, validator.createAccount)
  return body
}

async function deleteAccount (event) {
  console.log('Event: ', event)
  throw new Error('Forçando DLQ')
  return httpFactory('deleteAccount running', 200)
}


module.exports = {
  userPosition,
  createAccount,
  deleteAccount
}
