# desafio Técnico Toto Investimentos

## Requisitos (globais)
- NodeJs >14
- Serverless (usar npm)

## Passos para executar localmente
- Abra o projeto no VS Code
- Execute o comando "yarn" e aguarde a instalação das dependências
- Pressione "F5" e aguade a listagem de rotas

## Documentação
- https://roberto-developer.link/toro/index.html
